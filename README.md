# Vue Unicode Emoji

Simple unicode emojis by word.

## Example

### Vue Component

Input:

```html
<Emoji word="beer" />
```

Output:

```html
🍺
```

## Installation

`yarn add vue-unicode-emoji`

## Usage

Vue Template:

```html
<Emoji word="beer" />
```

Vue Script:

```javascript
import Emoji from 'vue-unicode-emoji';

export default {
  components: [Emoji]
};
```

May also use named imports:

```javascript
import { Emoji, emoji, lookup } from 'vue-unicode-emoji';

console.log(lookup['foggy']);

export default {
  components: [Emoji], // capital Emoji named export is the Vue component
  methods: {
    emoji // lowercase emoji named export is a lookup function
  }
};
```

```html
<Emoji word="beer" /> is {{ emoji('thumbsup') }}
```

## License

MIT
