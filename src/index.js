import emojiComponent from './Emoji.vue';
import emojiLookup from './lookup';

export const Emoji = emojiComponent;
export const lookup = emojiLookup;
export const emoji = function(word) {
  return emojiLookup[word];
};

export default emojiComponent;
